# plataforma-brasil-mobile

Esse repositório contém o build utilizado para gerar
os binários de cada plataforma mobile. Abaixo segue
os comandos executados para gerar o build.


```
git clone git@github.com:nucleo-digital/plataforma-brasil-clients.git
cd plataforma-brasil-clients
npm run-script mobile
cd build
git clone git@github.com:Wizcorp/phonegap-facebook-plugin.git
cordova -d plugin add phonegap-facebook-plugin --variable APP_ID="793593654040767" --variable APP_NAME="Plataforma Brasil"
cp ../../plataforma-brasil-mobile/config.xml .
adicionar 
    <script src="cordova.js"></script>
    <script src="cordova_plugins.js"></script>
no index.html
cordova run android
```
